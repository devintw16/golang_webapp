# golang_webapp



## Getting started

1. Build docker image locally:
   ```
    sudo docker build . -t golang_webapp
   ```
2. Run docker container to bring up webserver on port 8080 while using host networking mode:
   ```
    sudo docker run -itd --name devin_golang --network host golang_webapp
   ```
