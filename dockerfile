FROM golang:1.16-alpine

WORKDIR /app

COPY ./hello_go_http/* ./

RUN go build -o /hello_go_http

EXPOSE 8080

CMD [ "/hello_go_http" ]